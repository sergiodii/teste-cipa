const con = require('../server/db/conection');

module.exports = function(app){
    app.get('/status', function (req, res){
        let querySQL = 'SELECT * FROM candidato'
        con.query(querySQL, function(err, resultado){
            if (err) throw err;
            
            let compare = (a,b) => {
                if (a.voto < b.voto)
                   return 1;
                if (a.voto > b.voto)
                  return -1;
                return 0;}
            let apuracao = resultado.sort(compare)    
            res.render('../views/status', {candidato: apuracao});
        })
        
    });
};

/* TESTES DE SORT EM OBJETO
let tabela = [
    {
        id: 1,
        nome: 'joao',
        voto: 4,
    },
    {
        id: 2,
        nome: 'artur',
        voto: 6,
    },
    {
        id: 3,
        nome: 'maria',
        voto: 0,
    },
    {
        id:4,
        nome: 'caroline',
        voto: 10,
    },
    {
        id:5,
        nome: 'sergio',
        voto: 11,
    },
]

let compare = (a,b) => {
    if (a.voto < b.voto)
       return 1;
    if (a.voto > b.voto)
      return -1;
    return 0;}
tabela.sort(compare)
*/

