const con = require('../server/db/conection');

module.exports = function(app){
    app.get('/votacao', function (req, res){
        let querySQL = 'SELECT * FROM candidato'
        con.query(querySQL, function(err, resultado){
            if (err) throw err;          
            res.render('../views/votacao', {candidato: resultado});
        })
        
    });
};
