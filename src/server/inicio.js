const express = require('express');
const app = express();
var bodyparser = require('body-parser')
require('dotenv').load();

//DEFININDO BODYPARSER PARA PEGAR INFO DE FORM NO FRONT
//AND JSON TOO
app.use(bodyparser.urlencoded({extended: false}));
app.use(bodyparser.json());

//DEFININDO VIEW ENGINE COMO EJS E SETANDO O CAMINHO DAS VIEWS
app.set('views', __dirname + '../../views');
app.set('view engine', 'ejs')
//app.set('view options', {layout: false});

app.use(express.static(__dirname + '../../views'))

//INICIO DE ROTAS:

let pageIndex = require('../routes/start')
pageIndex(app);

let pageVotacao = require('../routes/votacao')
pageVotacao(app);

let pageStatus = require('../routes/status')
pageStatus(app);

let pageVotar = require('../routes/votar')
pageVotar(app);

//FIM DE ROTAS:


//SUBINDO SERVIDOR:
app.listen(process.env.PORTA, function(){
    console.log('SERVIDOR FUNCIONANDO NA PORTA :'+process.env.PORTA)
})